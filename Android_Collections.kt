// Introduction
fun Shop.getSetOfCustomers(): Set<Customer> =
        customers.toSet()



// Sort
fun Shop.getCustomersSortedByOrders(): List<Customer> =
        customers.sortedByDescending { it.orders.size }



//Filter map
fun Shop.getCustomerCities(): Set<City> =
        customers.map { it.city }.toSet()

fun Shop.getCustomersFrom(city: City): List<Customer> =
         customers.filter { it.city == city }

     

//All, Any, and other predicates
fun Shop.checkAllCustomersAreFrom(city: City): Boolean =
        customers.all { it.city == city }

fun Shop.hasCustomerFrom(city: City): Boolean =
        customers.any { it.city == city }

fun Shop.countCustomersFrom(city: City): Int =
		customers.count { it.city == city }

fun Shop.findCustomerFrom(city: City): Customer? =
        customers.find { it.city == city }



// Associate
fun Shop.nameToCustomerMap(): Map<String, Customer> =
        customers.associateBy(Customer::name)

fun Shop.customerToCityMap(): Map<Customer, City> =
        customers.associateWith(Customer::city)

fun Shop.customerNameToCityMap(): Map<String, City> =
        customers.associate { it.name to it.city }



//Group By
fun Shop.groupCustomersByCity(): Map<City, List<Customer>> =
		customers.groupBy { it.city }




//Partition
fun Shop.getCustomersWithMoreUndeliveredOrders(): Set<Customer> = customers.filter {
    val (delivered, undelivered) = it.orders.partition { it.isDelivered }
    undelivered.size > delivered.size
}.toSet()



//FlatMap
fun Customer.getOrderedProducts(): List<Product> =
     orders.flatMap(Order::products)

fun Shop.getOrderedProducts(): Set<Product> =
        customers.flatMap(Customer::getOrderedProducts).toSet()



//Max min
fun Shop.getCustomerWithMaxOrders(): Customer? =
        customers.maxByOrNull { it.orders.size }

fun getMostExpensiveProductBy(customer: Customer): Product? =
        customer.orders
                .flatMap(Order::products)
                .maxByOrNull(Product::price)




//Sum
fun moneySpentBy(customer: Customer): Double =
        customer.orders.flatMap { it.products }.sumOf { it.price }



// Fold and reduce
fun Shop.getProductsOrderedByAll(): Set<Product> =customers.map(Customer::getOrderedProducts).reduce { orderedByAll, customer ->
        orderedByAll.intersect(customer)
    }

fun Customer.getOrderedProducts(): Set<Product> =
    orders.flatMap(Order::products).toSet()



//Compound tasks
fun findMostExpensiveProductBy(customer: Customer): Product? {
    return customer
        .orders
        .filter(Order::isDelivered)
        .flatMap(Order::products)
        .maxByOrNull(Product::price)
}

fun Shop.getNumberOfTimesProductWasOrdered(product: Product): Int {
    return customers
            .flatMap(Customer::getOrderedProducts)
            .count { it == product }
}

fun Customer.getOrderedProducts(): List<Product> =
        orders.flatMap(Order::products)




//Sequences
fun findMostExpensiveProductBy(customer: Customer): Product? {
    return customer
            .orders
            .asSequence()
            .filter(Order::isDelivered)
            .flatMap(Order::products)
            .maxByOrNull(Product::price)
}

fun Shop.getNumberOfTimesProductWasOrdered(product: Product): Int {
     return customers
            .asSequence()
            .flatMap(Customer::getOrderedProducts)
            .count { it == product }
}

fun Customer.getOrderedProducts(): Sequence<Product> =
        orders.asSequence().flatMap(Order::products)




//Getting used to the new style
fun doSomethingWithCollection(collection: Collection<String>): Collection<String>? {
    val groupsByLength = collection.groupBy { s -> s.length }
    val maximumSizeOfGroup = groupsByLength.values.map { group -> group.size }.maxOrNull()
    return groupsByLength.values.firstOrNull { group -> group.size == maximumSizeOfGroup }
}
